bioApp.controller('surveyController', function($scope, $http, $rootScope, $sce) {
	if(local_survey)
	{

		$scope.questions = local_survey.questions;
		$scope.loading = false;
		$scope.surveyId = local_survey._id;
		$scope.title = local_survey.title;
		$scope.description = local_survey.description;
	}
	else
	{
		$scope.questions = [];
		$scope.loading = false;
		$scope.surveyId = null;
		$scope.title = "";
		$scope.description = "";
	}

	var question = function() {
		return {
			name: "",
			type: "txt",
			specify: false,
			answers: [],
			advancedSpecify: false,
			advanced_questions: [],
		};
	}

	var value = function() {
		return {name:"",specify: false};
	}
	
	$scope.addQuestion = function()
	{
		$scope.questions.push(new question());
	}

	$scope.removeQuestion = function()
	{
		$scope.questions = $scope.questions.splice(0,$scope.questions.length-1)
	}

	$scope.addAnswer = function(array)
	{
		array.push(new value());
	}
	$scope.removeAnswer = function(array,id)
	{
		array.splice(id, 1);
	}

	$scope.addAdvancedQuestion = function(questionsArray)
	{
		questionsArray.push(new question());
	}
	$scope.removeAdvancedQuestion = function(questionsArray,id)
	{
		console.log(questionsArray);
		questionsArray.splice(id, 1);
	}
	$scope.previewData = function(save)
	{
		if(!$scope.loading)
		{
			$scope.loading = true;
			data = {};
			data.questions = $scope.questions;
			data.title = $scope.title;
			data.description = $scope.description;
			var url = '/settings/survey/create';
			if($scope.surveyId != null)
				url = '/settings/survey/'+$scope.surveyId;

			if(!save)
				url += "?preview=true";

			$http.post(url,{survey:data})
			.success(function(returnData) {
				$scope.loading = false
				$scope.surveyId = null;
				if(returnData != "Error")
				{
					
					$scope.surveyId = returnData._id;
					if(save)
					{
						location.replace('/settings/surveys');
					}
					else
					{
						
						if($scope.tab == null || $scope.tab.window == null)
						{
							$scope.tab = window.open('about:blank', '_blank');
							$scope.tab.location.href = "/settings/survey/"+$scope.surveyId+"/preview";
						}
						else
						{
							$scope.tab.location.reload(true);
						}
					}
				}

			}).error(
				function(data){
					alert("500 - Error");
					console.log(data);
					$scope.loading = false;

				}
			)
		}
	}

	$scope.questionType = function(question)
	{
		if(question.type == "txt")
		{
			delete question.specify;
			delete question.answers;
			delete question.advancedSpecify;
			delete question.advanced_questions;
		}
		else
		{
			question.specify = false;
			question.answers = [];
			question.advancedSpecify = false;
			question.advanced_questions = [];
			
			if(question.answers.length < 1)
			{
				$scope.addAnswer(question.answers);
			}
		}
		console.log(question.answers);
	}
	if(local_survey == null)
		$scope.addQuestion();
});