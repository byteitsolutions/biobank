bioApp.controller('patientDetailsController', function($scope, $http) {
	$scope.surveys = surveys;
	
	$scope.deletePatient = function(nameenc, d, id)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		var name = decodeURIComponent(nameenc);
		deleteConfirm(name, function(result) {
			if(result)
				$http.get('/patient/'+id+'/delete/'+d).success(function(data) {
					location.replace('/patients');
				})
		})
	}

	deleteConfirm = function(name, callback)
	{
		bootbox.confirm("Do you wish to delete "+name+"?", function(result) {
			if(result)
			{
				bootbox.confirm("Are you sure, THIS WILL NEVER BE UNDONE!", function(result) {
					callback(result)
				}); 
			}
			else
			{
				callback(false)
			}
			
		}); 
	}

	$scope.removeImage = function(id)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		deleteConfirm("this consent form", function(result) {
			if(result)
				$http.get("/patient/image-upload/"+id+"/delete").success(function(data) {
					location.reload(true);
				})
		})
	}
});