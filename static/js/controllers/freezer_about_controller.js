bioApp.controller('freezerAboutController', function($scope, $http) {
	var colorArray = ["#00ff00","#2bff00","#55ff00","#80ff00","#aaff00","#d5ff00","#ffff00","#ffd500","#ffaa00","#ff8000","#ff5500","#ff2b00","#ff0000"];
	$scope.breadcrumbs = [{level:0,name:"Shelves"},{level:1,name:"Racks"},{level:2,name:"Drawers"},{level:3,name:"Boxes"},{level:4,name:"Samples"}]
	$scope.location = {};
	if(local_freezer)
	{
		$scope.freezer = local_freezer;
		freezer_id = local_freezer._id;
	}
	else
	{
		$scope.freezer = {};
	}
	$scope.sampletypes = sampletypes;
	$scope.level = 0;
	$scope.stats = [];
	$scope.statsLoading = false;

	$scope.downLevel = function(level, index)
	{
		$scope.level++;
		setLabel(level, index);
	}

	$scope.setLevel = function(lvl)
	{
		$scope.stats = null;
		$scope.level = lvl;
		if(lvl < 1)
			$scope.location.shelf = null;
		if(lvl < 2)
			$scope.location.rack = null;
		if(lvl < 3)
			$scope.location.drawer = null;
		if(lvl < 4)
			$scope.location.box = null;

		getStats();
	}
	setLabel = function(level, index)
	{

		index = index+1;
		switch(level)
		{
			case 0:
				$scope.location.shelf = index;
				break;
			case 1:
				$scope.location.rack = index;
				break;
			case 2:
				$scope.location.drawer = index;
				break;
			case 3:
				$scope.location.box = index;
				break;
		}
		console.log(level + " " + index);
		console.log($scope.location);
		if(level == 3)
		{
			getSamples();
		}
		else
			$scope.samples = null;

		getStats();
	}

	$scope.getColor = function(i)
	{
		var color = {"background-color": colorArray[colorArray.length-1]};
		if($scope.stats && $scope.stats[i])
		{
			per = $scope.stats[i].available;
			var index = Math.ceil((colorArray.length)*per);
			
			index = colorArray.length-index;

			if(colorArray[index])
				color["background-color"] = colorArray[index];
		}
		return color;
	}

	$scope.getStats = function(callback)
	{
		var url = '/api/freezer/all_space/'+freezer_id;
		if($scope.location.shelf)
			url += '/'+($scope.location.shelf-1);
		if($scope.location.rack)
			url += '/'+($scope.location.rack-1);
		if($scope.location.drawer)
			url += '/'+($scope.location.drawer-1);
		if($scope.location.box)
			url += '/'+($scope.location.box-1);
		$http.get(url).success(function(data) {
			callback(data);
		})
	}

	var getStats = function()
	{
		if(!$scope.statsLoading)
		{
			$scope.statsLoading = true;
			$scope.getStats(function(stats) {
				$scope.stats = stats;
				$scope.statsLoading = false;
			})
		}
	}

	var getSamples = function()
	{	var url = '/api/freezer/samples/'+freezer_id;
		url += '/'+($scope.location.shelf-1);
		url += '/'+($scope.location.rack-1);
		url += '/'+($scope.location.drawer-1);
		url += '/'+($scope.location.box-1);
		$http.get(url).success(function(data) {
			samplesCombine(data);
		})
	}
	var getAddress = function(data)
	{
		var url = '/api/freezer/address/'+freezer_id;
		$http.get(url).success(function(data) {
			$scope.address = data;
		})
	}

	var samplesCombine = function(samples)
	{
		var location = $scope.location;
		var multiArray = $scope.address[location.shelf-1][location.rack-1][location.drawer-1][location.box-1];

		for(var i = 0; i<samples.length; i++)
		{
			var sample = samples[i];
			multiArray[sample.location.box_location.x][sample.location.box_location.y].sample = sample;
		}
		$scope.samples = multiArray;
	}

	getStats();
	getAddress();

	$scope.reloadStats = function()
	{
		getStats();
	}

	$scope.setSample = function(sample)
	{
		$scope.pickedSample = sample;
		$scope.pickedDonation = null;

		$http.get('/api/donation/sample/'+sample._id).success(function(data) {
			$scope.pickedDonation = data;
		});
	}

	$scope.newDate = function(date)
	{
		return new Date(date).toDateString();
	}

});