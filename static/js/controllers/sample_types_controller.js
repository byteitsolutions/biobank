bioApp.controller('sampleTypesController', function($scope, $http) {
	$scope.newSample = function(data)
	{
		$scope.sampleTypeId = null;
		$scope.sample = {name:"", color:"#FFFFFF"}

		if(data != null)
		{
			$scope.sampleTypeId = data._id;
			$scope.sample = {name:data.name, color:data.color}
		}

		$("#sampleModal").modal('show');

		$scope.loading = false;
		$scope.update = (data != null);
	}

	$scope.createSample = function()
	{
		if(!$scope.loading)
		{
			$scope.loading = true;
			data = $scope.sample;
			var url = '/settings/sample/create';
			
			if($scope.sampleTypeId != null)
				url = '/settings/sample/'+$scope.sampleTypeId;

			$http.post(url,{sample:data})
			.success(function(returnData) {
				if(returnData != "Error")
				{
					reload();
				}
			}).error(
				function(data){
					alert("500 - Error");
					console.log(data);
					$scope.loading = false;

				}
			)
		}
	}
	$scope.removeSample = function(sample)
	{
		if(sample.count == 0)
		{
			var d = new Date().toDateString().split(" ").slice(1).join("-");
			console.log(sample);
			var name = sample.name;
			var id = sample._id;
			deleteConfirm(name, function(result) {
				if(result)
					$http.get('/settings/sample/'+id+'/delete/'+d).success(function(data) {
						reload();
					})
			})
		}
	}

	deleteConfirm = function(name, callback)
	{
		bootbox.confirm("Do you wish to delete "+name+"?", function(result) {
			if(result)
			{
				bootbox.confirm("Are you sure, THIS WILL NEVER BE UNDONE!", function(result) {
					callback(result)
				}); 
			}
			else
			{
				callback(false)
			}
			
		}); 
	}

	reload = function()
	{
		$http.get('/settings/samples/json').success(function(data){
			$scope.sample_types = data;
			$scope.loading = false;
			$("#sampleModal").modal('hide');
		})
	}
	$scope.reload = reload;
	$scope.sample_types = local_sample_types;
});