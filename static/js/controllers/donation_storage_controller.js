bioApp.controller('donationStorageController', function($scope, $http) {
	$scope.sampleTypes = local_sampletypes;
	$scope.sampleTypeAdd = local_sampletypes[0];
	$scope.samplesArr = [];
	$scope.samplesArrId = [];
	$scope.availableSpace = 0;

	$scope.filterFunction = function(item) {
		return $scope.samplesArrId.indexOf(item._id) == -1
	}
	$scope.disabledFunction = function() {
		for(var i = 0; i<$scope.samplesArr.length; i++)
		{
			var num = $scope.samplesArr[i].number
			if(isNaN(num) || num <= 0)
				return true;
		}
		return false;
	}
	$scope.addSample = function(index) {
		var obj = $scope.sampleTypeAdd;
		obj.number = 0;
		$scope.samplesArr.push(obj);
		$scope.samplesArrId.push(obj._id);


	}

	$scope.removeSample = function(index)
	{
		$scope.samplesArr.splice(index, 1);
		$scope.samplesArrId.splice(index, 1);
	}

	$scope.getTubeSizes = function(freezer)
	{
		if(freezer)
			// $http.get('/api/freezer/tubsizes/'+freezer).success(function(data) {
			// 	$scope.tubsizes = data;
			// })

			$scope.tubesizes = [{name:freezer, value:""}];
		else
			$scope.tubsizes = [];
	}

	$scope.getAvailableSpace = function(freezer,tube)
	{
		if(freezer && tube && tube._id)
			$http.get('/api/freezer/tubspace/'+freezer+'/'+tube._id).success(function(data)
			{
				$scope.availableSpace = data;
			});
		else
			$scope.availableSpace = 0;
	}
});