var login_check = require('./extensions/auth.js')
var auth = login_check.normal;
var admin = login_check.admin;

app.get('/dashboard',auth, routes.normal.dashboard.view);
app.get('/',auth, routes.normal.dashboard.view);

//--- Patient Routes ---\\\
app.get('/patients',auth, routes.normal.patients.view);
app.get('/patient/create',auth, routes.normal.patients.form);
app.post('/patient/create',auth, routes.normal.patients.create);

app.get('/patient/:id',auth, routes.normal.patients.details);
app.post('/patient/:id',auth, routes.normal.patients.update);
app.get('/patient/:id/delete/:date', auth, routes.normal.patients.delete)
app.get('/patient/:id/edit', auth, routes.normal.patients.form)
app.get('/patient/:id/json',auth, routes.normal.patients.json);
app.post('/patient/image-upload/:id', auth, routes.normal.patients.image.upload);
app.get('/patient/image-upload/:id/delete', auth, routes.normal.patients.image.remove);

app.get('/patient/donations/:id', auth, routes.normal.patients.donation.view);
app.get('/patient/donations/:id/delete/:date', auth, routes.normal.patients.donation.delete);


app.get('/freezers',auth, routes.normal.freezer.view);
app.get('/freezer/create',auth, routes.normal.freezer.form);
app.post('/freezer/create',auth, routes.normal.freezer.model.create);
app.post('/freezer/:id',auth, routes.normal.freezer.model.update);
app.get('/freezer/:id/edit',auth, routes.normal.freezer.form);

app.get('/freezer/:id', auth, routes.normal.freezer.about);
app.get('/freezer/:id/:shelf/', auth, routes.normal.freezer.about);
app.get('/freezer/:id/:shelf/:rack/', auth, routes.normal.freezer.about);
app.get('/freezer/:id/:shelf/:rack/:draw/', auth, routes.normal.freezer.about);
app.get('/freezer/:id/:shelf/:rack/:draw/:box/', auth, routes.normal.freezer.about);
app.get('/freezer/:id/:shelf/:rack/:draw/:box/:x/', auth, routes.normal.freezer.about);
app.get('/freezer/:id/:shelf/:rack/:draw/:box/:x/:y', auth, routes.normal.freezer.about);

//-- Add A Donation --\\
app.get('/donation/', auth, routes.normal.donation.form);
app.get('/donation/:patient', auth, routes.normal.donation.form);
app.post('/donation/create',auth, routes.normal.donation.model.create);
app.post('/donation/update/:id',auth, routes.normal.donation.model.update);
app.get('/donation/:id/delete/:date', auth, routes.normal.donation.model.delete);
app.get('/donation/:id/survey/add/:survey', auth, routes.normal.donation.survey.view);
app.post('/donation/:id/survey/add/:survey', auth, routes.normal.donation.survey.create);
app.get('/donation/:id/survey/remove/:survey_index/:survey_id/:date', auth, routes.normal.donation.survey.delete);
app.get('/donation/:id/survey/result/remove/:survey_index/:survey_id/:result_index/:result_id/:date', auth, routes.normal.donation.survey.result.delete);
app.get('/donation/:id/survey/result/find/:survey_index/:survey_id/:result_index/:result_id', auth, routes.normal.donation.survey.result.find);
app.post('/donation/:id/survey/result/add/:survey_index/:survey_id', auth, routes.normal.donation.survey.result.create);
app.post('/donation/:id/survey/result/update/:survey_index/:survey_id/:result_index/:result_id', auth, routes.normal.donation.survey.result.update);


// prints donations location table
app.get('/print',auth, routes.normal.sample_print.view);

app.get('/donation/:id/export', auth, routes.normal.donation.label.export);
app.get('/donation/:id/print', auth, routes.normal.donation.label.print);

app.get('/samples',auth, routes.normal.samples.view);
app.get('/sample/details/:id',auth, routes.normal.samples.sample.details);
app.post('/sample/create/:donationId',auth, routes.normal.samples.sample.create);
app.post('/sample/create/:donationId/single',auth, routes.normal.samples.sample.add);
app.post('/sample/update/:donationId/:id',auth, routes.normal.samples.sample.update);
app.get('/sample/delete/:donationId/:id/delete/:date', auth, routes.normal.samples.sample.delete)

app.get('/sample/:id/dataentry', auth, routes.normal.samples.dataentry.view);
app.get('/sample/:id/dataentry/:index/:entryid',auth,routes.normal.samples.dataentry.json);
app.get('/sample/:id/dataentry/:index/delete/:date', auth, routes.normal.samples.dataentry.delete);
app.post('/sample/:id/dataentry/:index/:entryid/update', auth, routes.normal.samples.dataentry.update);
app.post('/sample/:id/dataentry', auth, routes.normal.samples.dataentry.create);

// app.get('/samples/dataentry',auth, routes.normal.samples.dataentry);
// app.get('/samples/details',auth, routes.normal.samples.details);
// app.get('/samples/location',auth, routes.normal.samples.location);
// app.get('/samples/storage',auth, routes.normal.samples.storage);

app.post('/sample/lookup',auth, routes.normal.sample_lookup.results);
app.get('/sample/lookup',auth, routes.normal.sample_lookup.view);

app.post('/echo',auth, routes.debug.echo);


//--- Session Routes ---\\
//These Take care of login and out
app.get('/login', routes.sessions.view);
app.post('/login', routes.sessions.create);
app.get('/logout', routes.sessions.destroy);

//--- Routing For The samples ---\\
app.get('/settings/samples',auth, routes.normal.settings.samples.view);
app.get('/settings/samples/json',auth, routes.normal.settings.samples.json);
app.post('/settings/sample/create',auth, routes.normal.settings.sample.create);
app.post('/settings/sample/:id',auth, routes.normal.settings.sample.update);
app.get('/settings/sample/:id/delete/:date',auth, routes.normal.settings.sample.delete);


//---Routing for settings - tube sizes---\\
app.get('/settings/tubes',auth, routes.normal.settings.tubes.view);
app.get('/settings/tubes/json',auth, routes.normal.settings.tubes.json);
app.post('/settings/tube/create',auth, routes.normal.settings.tube.create);
app.post('/settings/tube/:id',auth, routes.normal.settings.tube.update);
app.get('/settings/tube/:id/delete/:date',auth, routes.normal.settings.tube.delete);



app.get('/settings/surveys',auth, routes.normal.settings.surveys.view);
app.get('/settings/surveys/json',auth, routes.normal.settings.surveys.json);
app.get('/settings/survey/create',auth, routes.normal.settings.survey.form);
app.post('/settings/survey/create',auth, routes.normal.settings.survey.create);
app.post('/settings/survey/:id',auth, routes.normal.settings.survey.update);

app.get('/settings/survey/:id/delete/:date',auth, routes.normal.settings.survey.delete)
app.get('/settings/survey/:id/update',auth, routes.normal.settings.survey.update_form);
app.get('/settings/survey/:id/preview',auth, routes.normal.settings.survey.preview);

app.get('/settings/users',admin, routes.normal.settings.users.view)
app.get('/settings/users/json',admin, routes.normal.settings.users.json);
app.post('/settings/user/create',admin, routes.normal.settings.user.create);
app.post('/settings/user/:id',admin, routes.normal.settings.user.update);
app.get('/settings/user/:id/delete/:date',admin, routes.normal.settings.user.delete);

app.get('/no_belong',auth, routes.normal.nobelong);

//app.get('*',auth, routes.redirect);


//--API ROUTES--\\

app.get('/api/donation/sample/:sample_id', auth, routes.api.donation.sample);
app.get('/api/sample/type/count/:type_id', auth, routes.api.sample.type.count);

//Returns list of samples given the address
app.get('/api/freezer/samples/:freezer_id', auth, routes.api.freezer.samples);
app.get('/api/freezer/samples/:freezer_id/:shelf/', auth, routes.api.freezer.samples);
app.get('/api/freezer/samples/:freezer_id/:shelf/:rack/', auth, routes.api.freezer.samples);
app.get('/api/freezer/samples/:freezer_id/:shelf/:rack/:draw/', auth, routes.api.freezer.samples);
app.get('/api/freezer/samples/:freezer_id/:shelf/:rack/:draw/:box/', auth, routes.api.freezer.samples);
app.get('/api/freezer/samples/:freezer_id/:shelf/:rack/:draw/:box/:x/', auth, routes.api.freezer.samples);
app.get('/api/freezer/samples/:freezer_id/:shelf/:rack/:draw/:box/:x/:y', auth, routes.api.freezer.samples);

//returns an object of the amount of space left in that address
app.get('/api/freezer/space/:freezer_id', auth, routes.api.freezer.space);
app.get('/api/freezer/space/:freezer_id/:shelf/', auth, routes.api.freezer.space);
app.get('/api/freezer/space/:freezer_id/:shelf/:rack/', auth, routes.api.freezer.space);
app.get('/api/freezer/space/:freezer_id/:shelf/:rack/:draw/', auth, routes.api.freezer.space);
app.get('/api/freezer/space/:freezer_id/:shelf/:rack/:draw/:box/', auth, routes.api.freezer.space);
app.get('/api/freezer/space/:freezer_id/:shelf/:rack/:draw/:box/:x/', auth, routes.api.freezer.space);
app.get('/api/freezer/space/:freezer_id/:shelf/:rack/:draw/:box/:x/:y', auth, routes.api.freezer.space);

app.get('/api/freezer/address/:freezer_id', auth, routes.api.freezer.address);

app.get('/api/freezer/all_space/:freezer_id', auth, routes.api.freezer.allSpace);
app.get('/api/freezer/all_space/:freezer_id/:shelf/', auth, routes.api.freezer.allSpace);
app.get('/api/freezer/all_space/:freezer_id/:shelf/:rack/', auth, routes.api.freezer.allSpace);
app.get('/api/freezer/all_space/:freezer_id/:shelf/:rack/:draw/', auth, routes.api.freezer.allSpace);
app.get('/api/freezer/all_space/:freezer_id/:shelf/:rack/:draw/:box/', auth, routes.api.freezer.allSpace);
app.get('/api/freezer/all_space/:freezer_id/:shelf/:rack/:draw/:box/:x/', auth, routes.api.freezer.allSpace);
app.get('/api/freezer/all_space/:freezer_id/:shelf/:rack/:draw/:box/:x/:y', auth, routes.api.freezer.allSpace);

app.get('/api/freezer/tubesizes/:id', auth, routes.api.freezer.tube.sizes);
app.get('/api/freezer/tubespace/:id/:tubesize', auth, routes.api.freezer.tube.space);

app.get('/api/search/query',auth,routes.api.search.query);


//app.all('*', auth, function(req,res) {res.redirect('/')});