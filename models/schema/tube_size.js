var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tubeSizeSchema = new Schema({
	name: String,
	description:String
});

// build and expose model
module.exports = mongoose.model('TubeSize', tubeSizeSchema);