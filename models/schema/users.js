var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var usersSchema = new Schema({
	username: {type: String, unique:true},
	password: String,
	description: String,
	surveys: [{type: Schema.Types.ObjectId, ref: 'Survey'}]
});

// build and expose model
module.exports = mongoose.model('Admin', usersSchema);