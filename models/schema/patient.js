var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var patientSchema = new Schema({
	firstname: String,
	lastname: String,
	hospital_number: String,
	address: String,
	dob: Date,
	consent: {
		hasImage: {type:Boolean, default: false},
		location: {type:String, default: ""},
		type: {type:String, default: ""}
	}
});
// build and expose model
module.exports = mongoose.model('Patient', patientSchema);