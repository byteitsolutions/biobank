var SampleType = require('../schema/sample_type.js');
var Donation = require('./donation.js');
var async = require('async');

var exports = require('../../extensions/crud_functions.js')(SampleType);
var functions = require('../../extensions/crud_functions.js')(SampleType);

exports.getAllWithNumbers = function (callback) {
	functions.getAll(function(sampleTypes) {
		async.map(sampleTypes, function(st, next){
			Donation.Sample.getNumType(st._id, function(count) {
				st.count = count;
				next(null, st);
			})
		}, function(err, types){
			console.log(types)
			callback(types);
		})
	})
}

exports.delete = function(date, id, callback)
{
	Donation.Sample.getNumType(id, function(count) {
		if(count == 0)
		{
			functions.delete(date, id, callback);
		}
		else
			callback(null);
	})
}

module.exports = exports;