var Patient = require('../schema/patient.js');
var Donation = require('../schema/donation.js');
var DonationFunctions = require('../functions/donation.js');

exports = require('../../extensions/crud_functions.js')(Patient);
functions = require('../../extensions/crud_functions.js')(Patient);

exports.Consent = require('./patient_consent.js');

exports.create = function(data, callback)
{
	data.dob = parseDate(data.dob);
	functions.create(data,callback);
}
exports.update = function(id, data, callback)
{
	data.dob = parseDate(data.dob);
	functions.update(id, data, callback)
}

exports.num_of_donations = function(id, callback)
{

}

parseDate = function(dtr)
{
	var d = dtr.split('-');

	if(d.length != 3)
		return new Date();

	return new Date(d[2], d[1]-1, d[0]);
}



exports.delete = function(date, id, callback)
{
	exports.Consent.delete(id, function(err, patient) {
		functions.delete(date, patient._id, callback);
	})
}


exports.Donation = {
	delete: function(date, id, callback)
	{
		var dateStr = new Date().toDateString().split(" ").slice(1).join("-");
		if(dateStr == date)
			Patient.findById(id, function(err, patient) {
				DonationFunctions.deleteByPatient(date, patient._id, function(err) {
					exports.delete(date, patient._id, callback);
				})
			});
		else
			callback("Not Here");
	}
}

module.exports = exports;