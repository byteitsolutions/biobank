module.exports =
	[
		{	
			imgloc:'patients.png',
			location:'/patients',
			tooltip:'Search, Add New Patient'
		},
		{	
			imgloc:'samples.png',
			location:'/samples',
			tooltip:'Complete Survey, Store Samples'
		},
		{	
			imgloc:'samplelookup.png',
			location:'/sample/lookup',
			tooltip:'Search Survey, Store Samples'
		},
		{	
			imgloc:'freezers.png',
			location:'/freezers',
			tooltip:'Add Freezer, Freezer Content'
		},
	];