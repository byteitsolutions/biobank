exports.echo = function(req,res)
{
	res.send(req.body);
}

exports.form = function(req, res)
{
	res.render('survey_form/before');
}


exports.preview = {

	create: function(req, res)
	{
		var survey = req.body.survey;

		if(survey == null)
		{
			req.session.survey = null;
		}
		else
		{
			req.session.survey = survey;
		}

		res.send("ok");
	},
	view: function(req, res)
	{
		if(req.session.survey)
		{
			if(req.params.preview == "json")
				res.send({survey:req.session.survey})
			else
				res.render('survey_form/preview', {survey: req.session.survey});
		}
		else
		{
			res.send("Err");
		}
	}
}
