var Patient = require("../../models/functions/patient.js");
var SampleType = require("../../models/functions/sample_type.js");
var Freezer = require("../../models/functions/freezer.js");
var Survey = require("../../models/functions/survey.js");
var Donation = require("../../models/functions/donation.js");
var json2csv = require("json2csv");

var model_routes = require("../../extensions/routing_functions.js")(Donation);

exports.form = function(req,res)
{
	Freezer.getAll(function(freezers) {
		SampleType.getAll(function(sampletypes) {
			Patient.Model.findById(req.params.patient, function(err, patient){
				if(patient)
				{
					res.render('donation/storage', {freezers:freezers, patient:patient, sampletypes:sampletypes});
				}
				else
				{
					res.render('donation/storage', {freezers:freezers, sampletypes:sampletypes});
				}
			})
		})
	});
}

model_routes.create =  function(req,res)
{
	var modelData = req.body;
	Donation.create(modelData, function(err, saved_model){
		if(err)
			res.send("Error");
		else
			res.redirect("/sample/details/"+saved_model._id)
	})
}

exports.label = {
	export: function(req,res)
	{
		Donation.getLabels(req.params.id, function(err, donation, labels) {
			var filename = "PrinterLabels_"+donation.date.toDateString().split(" ").join("-");
			filename += "_"+req.params.id;
			res.header({
				'Content-Disposition': "attachment;filename=\""+filename+".csv\"",
				'Contnet-Type': "text/csv"
			});

			json2csv({data: labels, fields: ['label']}, function(err, csv) {
				if (err) throw(err);
				res.send(csv);
			});
		})
	},
	print: function(req,res)
	{
		if(req.params.id)
			Freezer.getAll(function(freezers) {
				SampleType.getAll(function(sampletypes) {
					Donation.getOne(req.params.id, function(err, donation){
						if(donation)
						{
							res.render('donation/print', {donation:donation, sampletypes:sampletypes, freezers:freezers});
						}
						else
						{
							res.redirect("/samples")
						}
					})
				});
			});
		else
			res.redirect("/samples");
	}
}

exports.survey = {
	view: function(req,res)
	{
		if(req.params.id && req.params.survey)
			Survey.Model.findById(req.params.survey, function(err, survey){
				Donation.Model.findById(req.params.id, function(err, donation){
					if(donation && survey)
					{
						res.render('donation/survey_add', {donation:donation, survey:survey});
					}
					else
					{
						res.redirect("/samples")
					}
				})
			});
		else
			res.redirect("/samples");
	},
	create: function(req, res)
	{
		if(req.params.id && req.params.survey)
		{
			Donation.Survey.add(req.params.id, req.body, function(err, donation){
				if(donation)
				{
					console.log(donation.surveys);
					res.redirect('/sample/details/'+donation._id);
				}
				else
				{
					res.redirect('/samples');
				}
			})
		}
		else
			res.redirect("/samples");
	},
	delete: function(req, res)
	{
		Donation.Survey.delete(req.params.date, req.params.id, req.params.survey_index, req.params.survey_id, function(err, donation){
			if(donation)
			{
				res.send('ok')
			}
			else
			{
				res.redirect('/samples');
			}
		})
	},
	result: {
		delete: function(req, res)
		{
			Donation.Survey.Result.delete(req.params.date, req.params.id, req.params.survey_index, req.params.survey_id, req.params.result_index, req.params.result_id, function(err, donation){
				if(donation)
				{
					res.send('ok')
				}
				else
				{
					if(err) throw err;
					res.redirect('/samples');
				}
			})
		},
		create: function(req, res)
		{
			Donation.Survey.Result.create(req.params.id, req.params.survey_index, req.params.survey_id, req.body.result, function(err, donation){
				if(donation)
				{
					res.send('ok')
				}
				else
				{
					if(err) throw err;
					res.redirect('/samples');
				}
			})
		},
		find: function(req, res)
		{
			Donation.Survey.Result.find(req.params.id, req.params.survey_index, req.params.survey_id, req.params.result_index, req.params.result_id, function(err, result){
				if(result)
				{
					res.send(result);
				}
				else
				{
					if(err) throw err;
					res.redirect('/samples');
				}
			})
		},
		update: function(req, res)
		{
			Donation.Survey.Result.update(req.params.id, req.params.survey_index, req.params.survey_id,req.params.result_index, req.params.result_id, req.body.result, function(err, donation){
				if(donation)
				{
					res.send('ok')
				}
				else
				{
					if(err) throw err;
					res.redirect('/samples');
				}
			})
		},
	}
}

exports.model = model_routes;