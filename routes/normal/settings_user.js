var Admin = require("../../models/functions/admin.js");
exports.update = function(req,res)
{
	var userData = req.body.user;
	
	Admin.Model.findById(req.params.id, function(err, user){
		if(user)
		{
			Admin.update(user._id, userData, function(err, saved_user) {
				if(err)
					res.send(err);
				else
					res.send(saved_user);
			})
		}
	});
	
}
exports.create = function(req,res)
{
	var user = req.body.user;

	Admin.create(user, function(err, saved_user){
		if(err)
			res.send("Error");
		else
			res.send(saved_user);
	})
}

exports.delete = function(req, res)
{
	Admin.delete(req.params.date, req.params.id, function(output){
		res.send(output);
	})
}
