var Donation = require("../../models/functions/donation.js");

exports.view = function(req,res)
{
	res.render('sample_lookup/sample_lookup_view', {});
}
exports.results = function(req,res)
{
	Donation.Search.find(req.body.query, function(err, donations){
		res.render('sample_lookup/sample_lookup_results', {donations:donations});
	})
}