var Freezer = require("../../models/functions/freezer.js");


exports.samples = function (req, res) {
	Freezer.locations.findSamples(
	req.params.freezer_id,
	req.params.shelf,
	req.params.rack,
	req.params.draw,
	req.params.box,
	req.params.x,
	req.params.y,
	function(results){
		res.send(results);
	})
}

exports.space = function (req, res) {
	Freezer.locations.findSpace(
	req.params.freezer_id,
	req.params.shelf,
	req.params.rack,
	req.params.draw,
	req.params.box,
	req.params.x,
	req.params.y,
	function(results){
		res.send(results);
	})
}

exports.allSpace = function (req, res) {
	Freezer.locations.findAllSpace(
	req.params.freezer_id,
	req.params.shelf,
	req.params.rack,
	req.params.draw,
	req.params.box,
	req.params.x,
	req.params.y,
	function(results){
		res.send(results);
	})
}

exports.address = function(req, res)
{
	Freezer.locations.findAddress(
	req.params.freezer_id,
	function(results){
		res.send(results);
	})
}

exports.tube = {
	sizes: function(req, res)
	{
		Freezer.tube.sizes(req.params.id, function(result) {
			res.send(result);
		})
	},
	space: function(req, res)
	{
		Freezer.tube.space(req.params.id, req.params.tubesize, function(result) {
			res.send(result);
		})
	},
}