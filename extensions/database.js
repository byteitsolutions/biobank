var url = process.env.MONGO_URL || 'localhost';
url = url + '/biobank';
module.exports = require('mongoose').connect(url, function(err) {
	if (err)
	{ 
		console.log('Mongo connection error');
		console.log('Error: '+err);
	}
	else
	{
		console.log('Connected to Mongo - '+url)
	}
});